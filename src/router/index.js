import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Profile from '@/components/Profile'
import EditProfile from '@/components/EditProfile'
import AddBook from '@/components/AddBook'
import BookDetailed from '@/components/BookDetailed'
import ViewUser from '@/components/ViewUser'
import ViewBook from '@/components/ViewBook'
import EditBook from '@/components/EditBook'
import AddLoan from '@/components/AddLoan'
import ViewLoan from '@/components/ViewLoan'
import EditLoans from '@/components/EditLoans'
import AddBooking from '@/components/AddBooking'
import ViewBookingRecords from '@/components/ViewBookingRecords'
import EditBooking from '@/components/EditBooking'
import Timetable from '@/components/Timetable'
import LoanDetailed from '@/components/LoanDetailed'
import BookroomDetailed from '@/components/BookroomDetailed'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Register',
      name: 'Register',
      component: Register
    },
    {
      path: '/Profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/EditProfile',
      name: 'EditProfile',
      component: EditProfile
    },
    {
      path: '/AddBook',
      name: 'AddBook',
      component: AddBook
    },
    {
      path: '/ViewUser',
      name: 'ViewUser',
      component: ViewUser
    },
    {
      path: '/ViewBook',
      name: 'ViewBook',
      component: ViewBook
    },
    {
      path: '/EditBook',
      name: 'EditBook',
      component: EditBook
    },
    {
      path: '/AddLoan',
      name: 'AddLoan',
      component: AddLoan
    },
    {
      path: '/ViewLoan',
      name: 'ViewLoan',
      component: ViewLoan
    },
    {
      path: '/EditLoans',
      name: 'EditLoans',
      component: EditLoans
    },
    {
      path: '/BookDetailed',
      name: 'BookDetailed',
      component: BookDetailed
    },
    {
      path: '/AddBooking',
      name: 'AddBooking',
      component: AddBooking
    },
    {
      path: '/ViewBookingRecords',
      name: 'ViewBookingRecords',
      component: ViewBookingRecords
    },
    {
      path: '/EditBooking',
      name: 'EditBooking',
      component: EditBooking
    },
    {
      path: '/Timetable',
      name: 'Timetable',
      component: Timetable
    },
    {
      path: '/LoanDetailed',
      name: 'LoanDetailed',
      component: LoanDetailed
    },
    {
      path: '/BookroomDetailed',
      name: 'BookroomDetailed',
      component: BookroomDetailed
    }
  ]
})
